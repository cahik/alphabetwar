import unittest
from alphabetwar.priest.app.alphabet import alphabet_war


class TestPad651(unittest.TestCase):
    def test_alphabet(self):
        self.assertEquals(alphabet_war("tz"), "Left side wins!")
        self.assertEquals( alphabet_war("btbpbjdsmmpzqbt") , "Right side wins!" );
        self.assertEquals( alphabet_war("zt") , "Left side wins!" );
        self.assertEquals( alphabet_war("sj") , "Right side wins!" );
        self.assertEquals( alphabet_war("azt") , "Left side wins!" );
        self.assertEquals( alphabet_war("m") , "Right side wins!" );
        self.assertEquals( alphabet_war("jbdt") , "Let's fight again!" );
        self.assertEquals( alphabet_war("wololooooo") , "Left side wins!" );
        self.assertEquals( alphabet_war("zdqmwpbs") , "Let's fight again!" );
        self.assertEquals( alphabet_war("jmtwbtwjwjtdtbdjtmbtjqj") , "Left side wins!" );
