#===========================================================================================
#                                    versao 80%
#===========================================================================================
# def alphabet_war(entrada:str):
#     leftside = {"w": 4, "p": 3, "b": 2, "s": 1, "t":900000000000000000}
#     rightside = {"m": 4, "q": 3, "d": 2, "z": 1, "j":900000000000000000}
#     fight = []
#     # entrada = input(str("Let's fight! type here your letters:"))
#     right = []
#     left = []
#     priestleft = "Priest left said: 'Wo lo looooooo'"
#     priestright = "Priest right said: 'Wo lo looooooo'"
#     while True:
#         for i in entrada:
#             if i in leftside or i in rightside:
#                 fight.append(i)
#         print(fight)
#         # if "j" and "t" in fight:
#         #     priestright = False
#         #     priestleft = False
#         for i in fight:
#             if i in leftside:
#                 left.append(leftside[i])
#                 # if i == "t":
#                 #     priestleft = True
#             if i in rightside:
#                 right.append(rightside[i])
#                 # if i == "j":
#                 #     priestright = True
#
#         if sum(left) > sum(right):
#             return "Left side wins!"
#         if sum(right) > sum(left):
#             return "Right side wins!"
#         if sum(right) == sum(left):
#             return "Let's fight again!"
#
#         # print("left",left)
#         # print("right",right)
#         break



#================================================================================================================================================
#                                           Versão 100%
#================================================================================================================================================
def alphabet_war(entrada):

    leftside = [{"Nome":"w", "Valor":4}, {"Nome":"p", "Valor":3}, {"Nome":"b", "Valor":2}, {"Nome":"s", "Valor":1}, {"Nome":"t", "Valor":0}]
    rightside = [{"Nome":"m", "Valor":4}, {"Nome":"q", "Valor":3}, {"Nome":"d", "Valor":2}, {"Nome":"z", "Valor":1}, {"Nome":"j", "Valor":0}]
    entrada = list(" " + entrada + " ")
    right = []
    left = []


    for i in range(len(entrada)):
       if entrada[i] == "t":
           for g in range(len(rightside)):
               if rightside[g]["Nome"] == entrada[i+1]:
                   try:
                       if entrada[i + 2] == "j":
                           pass
                       else:
                           raise Exception("")
                   except:
                        entrada[i + 1] = leftside[g]["Nome"] if entrada[i+1] != "j" else "j"
               if rightside[g]["Nome"] == entrada[i-1]:
                    try:
                        if entrada[i-2] == "j":
                            pass
                        else:
                            raise Exception("")
                    except:
                        entrada[i - 1] = leftside[g]["Nome"] if entrada[i-1] != "j" else "j"

       if entrada[i] == "j":

           for g in range(len(leftside)):
                if leftside[g]["Nome"] == entrada[i+1]:
                   try:
                       if entrada[i + 2] == "t":
                           pass
                       else:
                           raise Exception("")
                   except:
                        entrada[i + 1] = rightside[g]["Nome"] if entrada[i+1] != "t" else "t"

                if leftside[g]["Nome"] == entrada[i - 1]:
                   try:
                       if entrada[i - 2] == "t":
                           pass
                       else:
                           raise Exception("")
                   except:
                        entrada[i - 1] = rightside[g]["Nome"] if entrada[i-1] != "t" else "t"


    entrada = "".join(entrada).strip()


    for letra in entrada:

        for dicio in leftside:
            if letra == dicio["Nome"]:
                left.append(dicio["Valor"])

        for dicio in rightside:
            if letra == dicio["Nome"]:
                right.append(dicio["Valor"])

    # print(entrada, "\n",left, "-"*30, right)
    if sum(left) > sum(right):
        return "Left side wins!"
    elif sum(right) > sum(left):
        return "Right side wins!"
    else:
        return "Let's fight again!"

print(alphabet_war("jtm"))
